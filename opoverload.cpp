#include <iostream>

using namespace std;

class Integer
{
    int i;

    public:
        Integer(int ii) : i(ii){}

        /* Copy constructor. Note that argument needs to be const */
        Integer(const Integer& arg){
            cout << "Calling Copy Constructor!" << endl;
            i = arg.i;
        }

        Integer operator+ (const Integer& rv) {
            cout << "Operator +" << endl;
            return Integer(i + rv.i);
        }

        void showme(void){
            cout << "This integer contains " << i << endl;
        }
};

int main(int argc, char** argv)
{
    Integer ii(10), jj(20), kk(30);

    ii.showme();
    jj.showme();

    kk = ii + jj;

    kk.showme();

    Integer test = ii;
    test.showme();

    return 0;
}