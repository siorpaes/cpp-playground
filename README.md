Copy constructor is called when:
-A class is created from another one: Cup mycup = anothercup;
-A class is passed by value to a function: dosomething(Cup acup);
-A class is created using the return value of a function: mycup = clonecups();

Note that if copy constructor is used on compiler generated temporary objects
then the argument of the copy contrcutor must be const because it does not make
sense to modify the value of a temporary created object.
Example:

Cup mycup = clonecup(anothercup);

The compiler will generate a temporary object to old the return value of clonecup()
and will use such object to copy-construct mycup. That, is, such temporary object
is passed to the copy-constructor. Hence, it must be const
