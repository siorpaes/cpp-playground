/** Multiset example
 * See https://qr.ae/TWHQvx
 */

#include <iostream>
#include <string>
#include <set>
 
using namespace std;

int main()
{
  multiset<string> theLines;
 
  string aLine;
  while(getline(cin, aLine))
    theLines.insert(aLine);
 
  for(string const& line : theLines)
    cout << line << '\n';
}


