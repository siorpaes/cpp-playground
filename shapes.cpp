/** \file shapes.cpp
 * C++ playground 
 */

#include <iostream>

using namespace std;

class point{
    int x;
    int y;
    static int refcount;

    public:
    point(int x, int y) : x(x), y(y) {
        cout << "constructing point with arguments " << x << " and " << y << endl;
        refcount++;
    }

    point() : x(0), y(0){
        cout << "constructing point with no arguments" << endl;
        refcount++;
    }

    ~point(){
        refcount--;
    }

    int showme(void){
        cout << __func__ << " Point: (" << x << ", " << y << ")" << " Refcount=" << refcount << endl;
        return 0;
    }
};

/* Initialize refcount to zero */
int point::refcount = 0;

class triangle{
    public:
    triangle() : rank(8), a(0, 0), b(1, 1), c(2, 2) {
        cout << "constructing triangle" << endl;
    }

    private:
    int rank;
    point a, b, c;
};

int main(int argc, char** argv)
{
    point a(10, 10);
    point b(20, 20);
    
    b.showme();
    b = a;
    b.showme();

    return 0;
}
