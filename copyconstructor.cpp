#include <iostream>

using namespace std;

class Cup{
    public:
        int height;
        bool empty;

        /* Constructors */
        Cup(int h, bool f) : height(h), empty(f){}
        Cup() : height(0), empty(true){}

        /* Copy Constructor. Note that argument must be const if returning Cup by value
         * See https://www.geeksforgeeks.org/copy-constructor-argument-const */
        Cup(const Cup& acup){
            cout << "Copy Constructor!!" << endl;
            height = acup.height;
            empty = acup.empty;
        }

        /* Show me */
        void showme(void){
            cout << height << ", " << empty << endl;
        }
};


/* Functions doing some stuff with classes */
bool isempty(Cup acup){
    return acup.empty;
}

/* Generates a Cup from global */
Cup bigcup;
Cup generateCupFromGlobal(void){
    bigcup.empty = false;
    return bigcup;
}

/* Generates a cup from function */
Cup generateCupFromFunction(void){
    Cup *cup = new Cup();
    return *cup;
}

/* Generates a cup from argument */
Cup clonecup(Cup acup){
    return acup;
}

int main(int argc, char** argv)
{
    /* Calls constructor */
    Cup mycup1;

    /* Calls constructor with arguments */
    Cup mycup2(10, false);

    /* Calls copy-constructor */
    cout << ">> Creating a class from another one" << endl;
    Cup mycup3 = mycup1;
    cout << "<< Creating a class from another one" << endl << endl;

    /* Does NOT call copy constructor, just bit copies */
    Cup mycup4;
    mycup4 = mycup1;

    /* Calls copy constructor as a Cup class is passed by value */
    cout << ">> passing Cup by argument" << endl;
    cout << "The cup is " << isempty(mycup4) << endl;
    cout << "<< passing Cup by argument" << endl << endl;

    cout << ">> Generating cup from function" << endl;
    Cup mycup5 = generateCupFromFunction();
    mycup5.showme();
    cout << "<< Generating cup from function" << endl << endl;

    cout << ">> Generating cup from global" << endl;
    Cup mycup6 = generateCupFromGlobal();
    mycup6.showme();
    cout << "<< Generating cup from global" << endl << endl;

    cout << ">> cloning cups" << endl;
    Cup mycup7 = clonecup(mycup1);
    cout << "<< cloning cups" << endl << endl;
    cout << "Original cup: "  << hex << &mycup1 << endl << "Cloned cup:   " << &mycup7 << endl;
    
    return 0;
}